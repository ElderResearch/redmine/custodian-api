/* ©2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.redmine.custodian.model;

import lombok.Builder;
import lombok.Getter;

@Getter @Builder
public class CacheReloadResponse {
	private String message;
	private int sizeBefore, sizeAfter;
}
