/* ©2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.redmine.custodian.caches;

import org.apache.commons.lang3.StringUtils;

import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.api.redmine.plugins.contacts.Contact;
import com.elderresearch.redmine.custodian.caches.RedmineCache.RedmineCRMCache;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class ContactCache extends RedmineCRMCache<Contact> {
	public ContactCache(RedmineAPI api) { super("contacts", api); }
	
	@Override
	protected double score(String s, Contact c) {
		return jw.apply(s, fullName(c));
	}

	@Override
	public void add(Contact c) {
		index(c, fullName(c));
	}
	
	@Override
	protected void doReload() {
		log.info("Loading existing contacts...");
		crm.contacts().paginator().forEach(c -> {
			if (c.getIsCompany() == Boolean.FALSE) { add(c); }
		});
	}

	public static String fullName(Contact c) {
		return StringUtils.normalizeSpace(String.join(StringUtils.SPACE,
			c.getFirstName(), c.getMiddleName(), c.getLastName()));
	}
}
