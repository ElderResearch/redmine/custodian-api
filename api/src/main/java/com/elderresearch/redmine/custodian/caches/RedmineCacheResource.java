/* ©2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.redmine.custodian.caches;

import java.util.List;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import org.jooq.lambda.Seq;

import com.elderresearch.redmine.custodian.model.CacheInfo;
import com.elderresearch.redmine.custodian.model.CacheReloadResponse;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;

import lombok.RequiredArgsConstructor;
import lombok.val;


@ResourceLabel("Cache Management")
@RequiredArgsConstructor
public class RedmineCacheResource {
    private final CacheRegistry registry;
    
    /**
     * Lists information about all the caches registered in this custodian.
     * @return a list of caches, including their name, the number of entries in each, and the time they were
     * last loaded (so entries created after that time may not be represented in the cache)
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<CacheInfo> list() {
    	return Seq.seq(registry.map().values()).map(RedmineCache::info).toList();
    }
    
    /**
     * Reloads the named cache. This removes all entries in memory and reloads them from the Redmine API.
     * @param cacheName the name of the cache to reload
     * @return information about the reload, including the number of entries before and after
     * @throws NotFoundException if no cache with the specified name is registered in this custodian
     */
	@POST
	@Path("/{cache}/reload")
    @Produces(MediaType.APPLICATION_JSON)
    public CacheReloadResponse reload(@PathParam("cache") String cacheName) {
        val cache = findCache(cacheName);
        int beforeCount = cache.size();
        cache.reload();
        return CacheReloadResponse.builder()
                .message("Cache " + cacheName + " successfully reloaded")
                .sizeBefore(beforeCount)
                .sizeAfter(cache.size())
                .build();
    }
	
	/**
	 * Gets the size, or the number of entries cached in memory, for the named cache.
	 * @param cacheName the name of the cache
	 * @return the cache size
	 * @throws NotFoundException if no cache with the specified name is registered in this custodian
	 */
	@GET
	@Path("/{cache}/size")
	@Produces(MediaType.TEXT_PLAIN)
	public int size(@PathParam("cache") String cacheName) {
		return findCache(cacheName).size();
	}
	
	private RedmineCache<?> findCache(String name) {
        val ret = registry.get(name);
        if (ret == null) {
        	throw new NotFoundException(String.format("Cache %s not found. Caches: %s", name, registry.cacheNames()));
        }
        return ret;
	}
}
