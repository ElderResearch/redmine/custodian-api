/* ©2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.redmine.custodian.caches;

import java.util.Map;
import java.util.Set;

import org.jooq.lambda.Seq;

import com.elderresearch.api.redmine.RedmineAPI;

import lombok.Getter;
import lombok.experimental.Accessors;

@Getter
@Accessors(fluent = true)
public class CacheRegistry {
    private Map<String, RedmineCache<?>> map;
	
    // Direct access to avoid lookups by name if using via the API
	private UserCache users;
	private ContactCache contacts;
    private CompanyCache companies;

    /**
     * Creates a cache registry with the standard caches (users, contacts, and companies)
     * as well as optionally additional caches.
     * @param api the API from which to query the entries
     * @param otherCaches optional other caches to register in this registry
     */
    public CacheRegistry(RedmineAPI api, RedmineCache<?>... otherCaches) {
    	init(Seq.of(otherCaches).concat(
    		users = new UserCache(api),
    		contacts = new ContactCache(api),
    		companies = new CompanyCache(api)
    	));
    }
    
    /**
     * Creates a cache registry with a custom list of caches. Note that if you use this
     * constructor, {@link #companies()}, {@link #contacts()}, and {@link #users()} will return {@code null}.
     * @param caches the list of caches to register
     */
    public CacheRegistry(Seq<RedmineCache<?>> caches) {
    	init(caches);
    }
    
    // Move to separate init() since we can't assign variables during a constructor
    private void init(Seq<RedmineCache<?>> caches) {
    	map = caches.toMap(RedmineCache::toString, $ -> $);
    	map.forEach((name, cache) -> cache.reload());
    }

    public int size() {
        return map.values().stream().mapToInt(RedmineCache::size).sum();
    }

    public Set<String> cacheNames() {
        return map.keySet();
    }

    public RedmineCache<?> get(String cacheName) {
        return map.get(cacheName);
    }
}
