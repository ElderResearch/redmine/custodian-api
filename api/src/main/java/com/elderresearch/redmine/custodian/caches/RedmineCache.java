/* ©2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.redmine.custodian.caches;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.similarity.JaroWinklerSimilarity;
import org.jooq.lambda.Seq;

import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.api.redmine.plugins.contacts.CRMAPI;
import com.elderresearch.commons.lang.LambdaUtils;
import com.elderresearch.redmine.custodian.model.CacheInfo;

import lombok.val;

public abstract class RedmineCache<T> {
    protected static final JaroWinklerSimilarity jw = new JaroWinklerSimilarity();

    private final String name;
    protected final RedmineAPI api;
    private final Map<String, T> map;
    private Date lastLoaded;
    
    protected RedmineCache(String name, RedmineAPI api) {
    	this.name = name;
    	this.api  = api;
    	this.map  = new HashMap<>();
	}
    
    public CacheInfo info() {
    	return CacheInfo.builder().name(name).size(size()).lastLoaded(lastLoaded).build();
    }

    /**
     * Return the "closeness" of the object to the query string. This is used for "fuzzy" matching of
     * objects in the cache. A {@code jw} object is in scope for Jaro-Winkler similarity. 
     * @param s the string query
     * @param t the object to match
     * @return a measure of how similar the object is to the query. A higher number means more similar.
     * @see JaroWinklerSimilarity
     */
    protected abstract double score(String s, T t);

    /**
     * Reload all objects from Redmine into the in memory cache.
     */
    public void reload() {
    	lastLoaded = new Date();
    	map.clear();
    	doReload();
    }
    
    protected abstract void doReload();

    /**
     * This method doesn't persist to Redmine, it just adds the object locally. This method should be used to prevent
     * any unnecessary calls to {@link #reload()}. Subclasses should use {@code index()}.
     * @param t Object to be added to the cache.
     */
    public abstract void add(T t);
    
    protected void index(T t, String... keys) {
    	if (t == null) { return; }
    	
    	for (val key : keys) {
    		LambdaUtils.accept(StringUtils.stripToNull(key), $ -> map.put($, t));	
    	}
    }

    /**
     * Find the object that most closely matches the query.
     * @param str the query
     * @param minScore the minimum similarity score. If no objects have a similarity score of greater than the
     * minimum threshold, {@code null} is returned.
     * @return the object that exactly matches the query, if one is found, or the most similar one, if one is
     * found with a score greater than {@code minScore}; otherwise, {@code null}.
     */
    public T find(String str, double minScore) {
        if (str == null) { return null; }
        
        val exactMatch = map.get(str);
        if (exactMatch != null) { return exactMatch; }

        T bestMatch = null;
        double bestScore = minScore;
        for (val c : map.values()) {
            val score = score(str, c);
            if (score > bestScore) {
                bestScore = score;
                bestMatch = c;
            }
        }
        return bestMatch;
    }

    public T findOrAdd(String s, double minScore, Supplier<T> doesNotExistSupplier) {
        T t = find(s, minScore);
        if (t == null) {
            t = doesNotExistSupplier.get();
            if (t != null) { add(t); }
        }
        return t;
    }

    public int size() { return map.values().size(); }
    
    @Override
    public String toString() { return name; }
    
    protected static double maxJW(String query, String... ids) {
    	return Seq.of(ids).filter(StringUtils::isNotBlank).mapToDouble(s -> jw.apply(query, s)).max().orElse(0.0);
    }
    
    static abstract class RedmineCRMCache<T> extends RedmineCache<T> {
    	protected final CRMAPI crm;
    	
    	protected RedmineCRMCache(String name, RedmineAPI api) {
    		super(name, api);
    		crm = new CRMAPI(api.client());
		}
    }
}
