/* ©2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.redmine.custodian.caches;

import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.api.redmine.plugins.contacts.Contact;
import com.elderresearch.redmine.custodian.caches.RedmineCache.RedmineCRMCache;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class CompanyCache extends RedmineCRMCache<Contact> {
	public CompanyCache(RedmineAPI api) { super("companies", api); }
	
    @Override
    protected double score(String s, Contact c) {
    	return maxJW(s, c.getFirstName(), c.getCompany());
    }
    
    @Override
    protected void doReload() {
        log.info("Loading existing companies...");
        crm.contacts().paginator().forEach(c -> { 
            if (c.getIsCompany() == Boolean.TRUE) { add(c); }
        });
    }

    @Override
    public void add(Contact c) {
    	index(c, c.getFirstName(), c.getCompany());
    }
}
