/* ©2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.redmine.custodian.caches;

import org.apache.commons.lang3.StringUtils;

import com.elderresearch.api.redmine.RedmineAPI;
import com.elderresearch.api.redmine.User;
import com.elderresearch.api.redmine.User.UserStatus;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class UserCache extends RedmineCache<User> {
	public UserCache(RedmineAPI api) { super("users", api); }
	
	@Override
	protected double score(String s, User u) {
		// We indexed login and email as exact matches, so they will still work. Only fuzzy match on name.
		return jw.apply(s, fullName(u));
	}

	@Override
	protected void doReload() {
		log.info("Loading users...");
		api.users().paginator(UserStatus.all()).forEach(this::add);
	}

	@Override
	public void add(User u) {
		index(u, u.getLogin(), u.getMail(), fullName(u));
	}

	private static String fullName(User u) {
		return fullName(u.getFirstname(), u.getLastname());
	}
	private static String fullName(String f, String l) {
		return StringUtils.normalizeSpace(String.join(StringUtils.SPACE, f, l));
	}
}
