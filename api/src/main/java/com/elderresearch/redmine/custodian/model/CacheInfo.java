/* ©2021 Elder Research, Inc. All rights reserved. */
package com.elderresearch.redmine.custodian.model;

import java.util.Date;

import lombok.Builder;
import lombok.Getter;

@Getter @Builder
public class CacheInfo {
	private String name;
	private int size;
	private Date lastLoaded;
}
